#!/usr/bin/env python3

import os
import argparse
import subprocess
import json
import csv
import xmltodict
import numbers
import spectral.io.envi as envi
import geopandas as gpd
import numpy as np
import pyproj
from osgeo import gdal
import matplotlib.pyplot as plt
from time import process_time
from datetime import datetime
# from pyproj import Transformer


def main():

    # load context.json
    ctx = load_context()
    search_coordinates = ctx.get("search_coordinates")
    granule_folder = ctx.get("localize_urls")[0].get("url").split("/")[-1]
    granule_name = ctx.get("granule_name")
    granule_location = "./input/{}/{}".format(granule_folder,granule_name)

    # Start the stopwatch / counter 
    # t1_start = process_time() 

    # create csv file
    output_csv = 'output_{}_{}.csv'.format(granule_name, datetime.now().strftime('%Y-%m-%dT%H:%M:%S.%f')[:-3] + "Z")
    f = open(output_csv, 'w')
    writer = csv.writer(f)

    # get headers
    headers = get_headers(granule_location)
    writer.writerow(headers)

    if len(search_coordinates) == 1:     
        point = search_coordinates[0]

        # run gdalloactioninfo cli command
        gdal_info = execute_gdalloactioninfo(granule_location, [point[0], point[1]])

        # get inputs
        inputs = [granule_name.split("/")[-1], point[0], point[1]]

        # get band values
        bands = get_band_values(gdal_info)

        # input to csv file
        writer.writerow(inputs + bands)


    elif len(search_coordinates) > 1:
        # get list of analyzed points
        data_points = get_polygon_data_points(ctx)        

        # get bands for each data point
        for point in data_points:
            gdal_info = execute_gdalloactioninfo(granule_location, point)
            bands = get_band_values(gdal_info)
            inputs = [granule_name.split("/")[-1], point[0], point[1]]
            writer.writerow(inputs + bands)

        # get bands for each data point
        # output = np.apply_along_axis(get_point_values, 1, data_points)
        # for row in output:
        #     writer.writerow(inputs + bands)

    # close the file
    f.close()    

    # move csv to output directory
    if not os.path.exists('output'):
        os.makedirs('output')
    os.replace("./{}".format(output_csv), "./output/{}".format(output_csv))

    # Stop the stopwatch / counter
    # t1_stop = process_time()
    
    # print("Elapsed time:", t1_stop, t1_start) 
    
    # print("Elapsed time during the whole program in seconds:",
    #                                         t1_stop-t1_start) 


def get_point_values(file_location, point):
    gdal_info = execute_gdalloactioninfo(file_location, point)
    bands = get_band_values(gdal_info)
    inputs = [file_location.split("/")[-1], point[0], point[1]]
    return inputs+bands


def execute_gdalloactioninfo(file_location, point):
    # run gdalloactioninfo cli command
    gdal_command = "gdallocationinfo {} -wgs84 {} {} -xml".format(file_location, point[0], point[1]) # gdallocationinfo f130612t01p00r05_corr_v1a_img_topo_brdf_Lignin -wgs84 -118.8899 37.74691 -xml
    output = os.popen(gdal_command).read()
    output_json = json.dumps(xmltodict.parse(output))
    return output_json


def get_band_values(gdal_output):
    bands = []
    for band in json.loads(gdal_output)["Report"]["BandReport"]:
        bands.append(band["Value"])
    return bands


def get_headers(file_location):
    # constant headers
    headers = ["PlotName", "Lat", "Lon"]

    # find header file for input file
    if file_location.split("/")[-1].find(".hdr") == -1:
        file_location = file_location + ".hdr"

    if os.path.exists(file_location):
        # get header file information    
        hdr = envi.read_envi_header(file_location)
        return headers + hdr["band names"]
    else:
        raise RuntimeError("Could not find {} hdr file".format(file_location.split("/")[-1]))


# def convert_geojson_to_shpfile():
#     gdf = gpd.read_file('file.geojson')
#     gdf.to_file('file.shp')


def project_coords(coords, from_proj=pyproj.Proj(init='epsg:4326'), to_proj=pyproj.Proj(init='epsg:32753')):
    if len(coords) < 1:
        return []

    if isinstance(coords[0], numbers.Number):
        from_x, from_y = coords
        to_x, to_y = pyproj.transform(from_proj, to_proj, from_x, from_y)
        return [to_x, to_y]

    new_coords = []
    for coord in coords:
        new_coords.append(project_coords(coord, from_proj, to_proj))
    return new_coords


def get_polygon_data_points(ctx):

    # input_image = './test/indonesia_test/indonesia' # input product
    # input_geojson = './test/indonesia_test/indonesia_poly.geojson' # input point/polygon
    # output_mask = './test/indonesia_test/indonesia_polygons.tif'

    granule_name = ctx.get("granule_name") # input product
    granule_folder = ctx.get("localize_urls")[0].get("url").split("/")[-1]
    granule_location = "./input/{}/{}".format(granule_folder,granule_name)
    input_geojson = ctx.get("search_coordinates") # input point/polygon
    output_mask = granule_name + '.tif'

    image  = gdal.Open(granule_location)
    xmin,pixel_size,a,ymax,c,d =  image.GetGeoTransform()

    # convert coordinates from WGS84 to UTM
    targ_proj = pyproj.Proj(image.GetProjection())
    t_srs_code = targ_proj.crs.to_epsg()
    utm_geojson = project_coords(input_geojson, to_proj=pyproj.Proj(init='epsg:{}'.format(t_srs_code)))

    # input geojson to file
    geojson_file = 'input_geojson.json'
    with open(geojson_file, 'w') as f:
        json.dump({"type":"Feature","properties":{},"geometry":{"type":"Polygon","coordinates": [utm_geojson]}}, f)

    # rasterize image
    args = {}
    # args['layer_name'] = input_image
    args['geojson'] = geojson_file
    # args['field'] ='poly_id'
    args['output_mask'] =output_mask
    args['lines'] =image.RasterYSize
    args['columns'] =image.RasterXSize
    args['xmin'] = xmin
    args['ymax'] = ymax
    args['xmax'] = xmin + pixel_size * args['columns']
    args['ymin'] = ymax - pixel_size * args['lines']


    # gdal_template = '''gdal_rasterize -l {layer_name} -a {field} -ts {columns} {lines} -a_nodata -1.0 -te {xmin} {ymin} {xmax} {ymax} -ot Float32 -of GTiff {geojson} {output_mask}'''
    gdal_template = '''gdal_rasterize -burn 0 -ts {columns} {lines} -a_nodata -1.0 -te {xmin} {ymin} {xmax} {ymax} -ot Float32 -of GTiff {geojson} {output_mask}'''
    gdal_cmd = gdal_template.format(**args)
    os.system(gdal_cmd)

    polygon_mask = gdal.Open(output_mask).ReadAsArray() == 0

    # extract lat lon points
    lines,columns =  np.indices(polygon_mask.shape)
    easting = xmin + columns *pixel_size + pixel_size/2
    northing = ymax - lines *pixel_size - pixel_size/2
    in_crs = pyproj.Proj("EPSG:%s" % (t_srs_code))
    out_crs= pyproj.Proj("EPSG:4326")
    longitude,latitude= pyproj.transform(in_crs,out_crs,easting,northing)
    longitude = longitude[polygon_mask]
    latitude = latitude[polygon_mask]

    #Sampled pixels
    data = []
    for x in range(len(longitude)):
        data.append([latitude[x], longitude[x]]) 

    return data


def load_context():
    '''loads the context file into a dict'''
    try:
        context_file = '_context.json'
        with open(context_file, 'r') as fin:
            context = json.load(fin)
        return context
    except:
        raise Exception('unable to parse _context.json from work directory')


# def parser():
#     '''
#     Construct a parser to parse arguments
#     @return argparse parser
#     '''
#     parser = argparse.ArgumentParser(description="Run gdalloactioninfo commmand over ")
#     parser.add_argument('-i', '--image', type=str, help='path to image file', dest='image', required=True)
#     parser.add_argument('-la', '--lat', type=str, help='lat point', dest='lat', required=False)
#     parser.add_argument('-lo', '--lon', type=str, help='lon point', dest='lon', required=False)
#     parser.add_argument('-geo', '--geojson', type=str, help='path to geojson file', dest='geojson', required=False)
#     return parser


if __name__ == '__main__':
    # args = parser().parse_args()
    main()
