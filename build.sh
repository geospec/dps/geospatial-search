#!/usr/bin/env bash

set -ex

VERDI_HOME=/home/gitlab-runner/verdi
REPO_NAME=geospatial-search
source ${VERDI_HOME}/bin/activate
export SKIP_PUBLISH="noskip"
isSet=1
for var in S3_CODE_BUCKET MOZART_REST_URL GRQ_REST_URL
do
    if [ -z "${!var}" ]; then
        echo "${var} not set"
        isSet=""
    fi
done
if [ -z "${isSet}" ]; then
    echo "One or more variable is not set"
    exit 1
fi
echo 'listing variables'
echo ${CI_REPOSITORY_URL}
echo ${CI_COMMIT_REF_NAME}
echo ${SKIP_PUBLISH}
CONTAINER_REGISTRY="localhost:5050"
echo ${CONTAINER_REGISTRY}
echo 'end of listing variables'
${VERDI_HOME}/ops/container-builder/build-container.bash ${REPO_NAME} ${CI_COMMIT_REF_NAME} s3s://s3-us-west-2.amazonaws.com/geospec-code-bucket-dev https://172.31.19.92/mozart/api/v0.1/ http://172.31.25.138:8878/api/v0.1 ${SKIP_PUBLISH} "" --build-arg REPO_NAME=${REPO_NAME} --build-arg BRANCH=${CI_COMMIT_REF_NAME}
