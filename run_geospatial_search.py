"""
Wrapper script to call geospatial_search.py
"""
import json
import os
import logging
import sys
import geospatial_search

logging.basicConfig(
        stream=sys.stdout,
        level=logging.INFO,  # TODO set it from container level
        format="%(asctime)s [%(levelname)s] [%(name)s::%(lineno)d] %(message)s",
    )
logger = logging.getLogger(__name__)

if __name__ == '__main__':
    geojson = dict()
    lat = None
    lon = None
    args = dict()
    # reading context
    f = open("_context.json").read()
    context = json.loads(f)
    # reading in inputs to job
    granule_url = context.get("localize_urls")[0].get("url")
    granule_name = context.get("granule_name")
    coordinates = context.get("search_coordinates")
    if type(coordinates) is str:
        coordinates = json.loads(coordinates)
    # setting geojson / lat and lon based on polygon or point input
    if len(coordinates) == 1:
        lat = coordinates[0][0]
        lon = coordinates[0][1]
        args[lat] = lat
        args[lon] = lon
    elif len(coordinates) > 1:
        geojson = {
            "type": "Feature",
            "geometry": {
                       "type": "Polygon",
                       "coordinates": [
                             coordinates
                       ]
                   }
            }
        args[geojson] = geojson
    else:
        raise RuntimeError("Not enough points in input coordinates: {}".format(coordinates))

    # creating path to input granule
    dataset_dir = os.path.basename(granule_url)
    image = os.path.join(dataset_dir, granule_name)
    args["image"] = image
    logger.info("constructed arguments for geospatial search\n {}".format(json.dump(args)))
    # Call the geospatial_search script
    geospatial_search.main(**args)

