#!/bin/bash

basedir=$( cd "$(dirname "$0")" ; pwd -P )
source activate base
mamba env update --file ${basedir}/env.yml
